﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HiBlog.Startup))]
namespace HiBlog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
