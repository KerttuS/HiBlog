﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HiBlog.Models;

namespace HiBlog.Controllers
{
    public class TagPostsController : Controller
    {
        private HiBlogContext db = new HiBlogContext();

        // GET: TagPosts
        public ActionResult Index()
        {
            var tagPosts = db.TagPosts.Include(t => t.Post).Include(t => t.Tag);
            return View(tagPosts.ToList());
        }

        // GET: TagPosts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TagPost tagPost = db.TagPosts.Find(id);
            if (tagPost == null)
            {
                return HttpNotFound();
            }
            return View(tagPost);
        }

        // GET: TagPosts/Create
        public ActionResult Create()
        {
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title");
            ViewBag.TagId = new SelectList(db.Tags, "Id", "Name");
            return View();
        }

        // POST: TagPosts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PostId,TagId")] TagPost tagPost)
        {
            if (ModelState.IsValid)
            {
                db.TagPosts.Add(tagPost);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", tagPost.PostId);
            ViewBag.TagId = new SelectList(db.Tags, "Id", "Name", tagPost.TagId);
            return View(tagPost);
        }

        // GET: TagPosts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TagPost tagPost = db.TagPosts.Find(id);
            if (tagPost == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", tagPost.PostId);
            ViewBag.TagId = new SelectList(db.Tags, "Id", "Name", tagPost.TagId);
            return View(tagPost);
        }

        // POST: TagPosts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PostId,TagId")] TagPost tagPost)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tagPost).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", tagPost.PostId);
            ViewBag.TagId = new SelectList(db.Tags, "Id", "Name", tagPost.TagId);
            return View(tagPost);
        }

        // GET: TagPosts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TagPost tagPost = db.TagPosts.Find(id);
            if (tagPost == null)
            {
                return HttpNotFound();
            }
            return View(tagPost);
        }

        // POST: TagPosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TagPost tagPost = db.TagPosts.Find(id);
            db.TagPosts.Remove(tagPost);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
