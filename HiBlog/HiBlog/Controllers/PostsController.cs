﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HiBlog.Models;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;


namespace HiBlog.Controllers
{
    public class PostsController : Controller
    {
        private HiBlogContext db = new HiBlogContext();

        // GET: Posts
        //public ActionResult Index()
        //{
        //    var posts = db.Posts.OrderByDescending(x => x.PostedOn).Take(5);
        //    return View(posts.ToList());
        //    return View(db.Posts.ToList());
        //}
        
        public ActionResult ShowAllPosts()
        {
            if (User.IsInRole("Moderaator"))
            {
                return View(db.Posts.Include("User").ToList());
            }
            return View(db.Posts.Include("User").Where(x => x.Published).ToList());
        }

        public ActionResult ShowMyPosts()
        {
            if (User.IsInRole("Blogger") || User.IsInRole("Moderaator"))
            {
                return View("ShowAllPosts", db.Posts.Include("User").ToList().Where(x => x.UserId == User.Identity.GetUserId()).ToList());
            }
            return RedirectToAction("Index");
        }

        // GET: Posts/Details/5
        public ActionResult ReadOn(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var post = db.Posts.Include("TagPost.Tag").Where(x => x.Id == id).FirstOrDefault();
            IQueryable<Tag> tags = db.Tags;

            tags = db.Tags.Include("TagPost").Where(p => p.TagPost.Any(r => r.PostId == post.Id));
            tags.ToList();
            

            if (post == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserId = User.Identity.GetUserId();
            return View(post);

        }

        // GET: Posts/Create
        public ActionResult Create()
        {
            if(User.IsInRole("Blogger") || User.IsInRole("Moderaator"))
            {
                ViewBag.TagId = new SelectList(db.Tags, "Id", "Name", "Vali silt");
                return View();
            }
            return RedirectToAction("Register", "Account");
        }

        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Text,Published,PostedOn")] Post post, int? tagId)
        {
            if (ModelState.IsValid)
            {
               
                //post.Modified = DateTime.Now;
                if(User.Identity.IsAuthenticated)
                {
                    post.UserId = User.Identity.GetUserId();
                    var log = new Log() { Action = "Post created", AppUserId = User.Identity.GetUserId(), TimeOfAction = DateTime.Now };
                    db.Logs.Add(log);
                }
                
                post.PostedOn = DateTime.Now;
                db.Posts.Add(post);
                db.SaveChanges();

                if (tagId != null)
                {
                    var id = post.Id;
                    var tagPost = new TagPost() { PostId = id, TagId = (int)tagId };
                    db.TagPosts.Add(tagPost);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            return View(post);
        }

        public ActionResult Edit(int? id)
        {
            
            ViewBag.TagId = new SelectList(db.Tags, "Id", "Name");
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Post post = db.Posts.Find(id);
            post = db.Posts.Include("TagPost.Tag").Where(x => x.Id == id).FirstOrDefault();
            IQueryable<Tag> tags = db.Tags;

            tags = db.Tags.Include("TagPost").Where(p => p.TagPost.Any(r => r.PostId == post.Id));
            tags.ToList();

            // Siin tehtud ja välja kommenteeritud rippmenüü siltidest, mis antud postitusega seotud
            //ViewBag.TagPost = new SelectList(db.Tags.Include("TagPost").Where(p => p.TagPost.Any(r => r.PostId == post.Id)), "Id", "Name", "Vali silt");

            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Text,Published,PostedOn,Modified,UserId")] Post post, int? tagId)
        {
            if (ModelState.IsValid)
            {
                if (User.Identity.IsAuthenticated)
                {
                    var log = new Log() { Action = "Changed post", AppUserId = User.Identity.GetUserId(), TimeOfAction = DateTime.Now };
                    db.Logs.Add(log);
                }
                post.PostedOn = DateTime.Now; ;
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();

                if (tagId != null)
                {
                    var id = post.Id;
                    var tagPost = new TagPost() { PostId = id, TagId = (int)tagId };
                    db.TagPosts.Add(tagPost);
                    db.SaveChanges();
                }
                
                return RedirectToAction("Index");
            }
            return View(post);
        }
        
        [ActionName("DeleteTagFromPost")]
        public ActionResult DeleteConfirmedTag(int postId, int tagId)
        {
            var tagPost = db.TagPosts.Where(x => x.PostId == postId && x.TagId == tagId).FirstOrDefault();

            db.TagPosts.Remove(tagPost);
            db.SaveChanges();
            
            return RedirectToAction("Edit", new { id = postId });
        }

        // GET: Posts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var log = new Log() { Action = "Deleted post", AppUserId = User.Identity.GetUserId(), TimeOfAction = DateTime.Now };
                db.Logs.Add(log);
            }

            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ViewResult Index(string searchString, int? tagId)
        {
            ViewBag.TagId = new SelectList(db.Tags, "Id", "Name", "Vali silt");

            IQueryable<Post> posts = db.Posts;

            if(tagId != null)
            {
               posts = posts.Include("TagPost").Where(p => p.TagPost.Any(r => r.TagId == tagId));
            }

            posts = posts.Where(x => x.Published).OrderByDescending(x => x.PostedOn).Take(5);

            //if (searchString == null)
            //{
            //    posts = posts.OrderByDescending(x => x.PostedOn).Take(5);
            //    return View(posts.ToList());
            //}

            ViewData["CurrentFilter"] = searchString;
            
            if (!String.IsNullOrEmpty(searchString))
            {
                posts = posts.Where(s => s.Text.Contains(searchString)
                                       || s.Title.Contains(searchString) || s.User.FirstName.Contains(searchString));
               
            }

            return View(posts.ToList());
        }

        public ActionResult SortByTag(int? tagId)
        {
            IQueryable<Post> posts = db.Posts;
            if (tagId != null)
            {
                posts = posts.Include("TagPost").Where(p => p.TagPost.Any(r => r.TagId == tagId));
            }
            return View(posts.ToList());
        }

        public ActionResult AddLike (int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var log = new Log() { Action = "Added \"Like\"", AppUserId = User.Identity.GetUserId(), TimeOfAction = DateTime.Now };
                db.Logs.Add(log);
            }
            var post = db.Posts.Find(id);
            post.NumberOfLikes++;
            db.Entry(post).State = EntityState.Modified;
            db.SaveChanges();
            
            return RedirectToAction("ReadOn", new { id = post.Id});
        }

        //public ActionResult ShowAllPostsByNumberOfLikes()
        //{
        //    return View(db.Posts.OrderByDescending(x => x.NumberOfLikes).ToList());
        //}

        public ActionResult SortBy(string sortOrder)
        {
            List<Post> posts;

            if (User.IsInRole("Admin") || User.IsInRole("Moderaator"))
            {
                
                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Title" : "";
                ViewBag.DateSortParm = sortOrder == "PostedOn" ? "NumberOfLikes" : "PostedOn";
                posts = db.Posts.Include("User").ToList();

                switch (sortOrder)
                {
                    case "Title":
                        posts = posts.OrderBy(s => s.Title).ToList();
                        break;
                    case "PostedOn":
                        posts = posts.OrderByDescending(s => s.PostedOn).ToList();
                        break;
                    case "NumberOfLikes":
                        posts = posts.OrderByDescending(s => s.NumberOfLikes).ToList();
                        break;
                    case "Published":
                        posts = posts.OrderByDescending(s => s.Published).ToList();
                        break;
                    default:
                        posts = posts.OrderBy(s => s.PostedOn).ToList();
                        break;
                }
                TempData["SortedPosts"] = posts;
                return RedirectToAction("ShowAllPosts");
            }
            
            
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Title" : "";
            ViewBag.DateSortParm = sortOrder == "PostedOn" ? "NumberOfLikes" : "PostedOn";
            posts = db.Posts.Include("User").ToList();

            switch (sortOrder)
            {
                case "Title":
                    posts = posts.Where(x => x.Published).OrderByDescending(s => s.Title).ToList();
                    break;
                case "PostedOn":
                    posts = posts.Where(x => x.Published).OrderBy(s => s.PostedOn).ToList();
                    break;
                case "NumberOfLikes":
                    posts = posts.Where(x => x.Published).OrderByDescending(s => s.NumberOfLikes).ToList();
                    break;
                default:
                    posts = posts.Where(x => x.Published).OrderBy(s => s.PostedOn).ToList();
                    break;
            }
            TempData["SortedPosts"] = posts;
            return RedirectToAction ("ShowAllPosts");

        }
    }
}
