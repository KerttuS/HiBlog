﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace HiBlog.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }


        [Required(ErrorMessage = "Nime sisestamine on nõutav!")]
        [Display(Name = "Eesnimi")]
        public string FirstName { get; set; }

        [Display(Name = "Perekonna nimi")]
        public string LastName { get; set; }

        public ICollection<Post> Posts { get; set; }

        public ICollection<Log> Logs { get; set; }

       

    }

    public class HiBlogContext : IdentityDbContext<ApplicationUser>
    {
        public HiBlogContext()
            : base("HiBlogContext", throwIfV1Schema: false)
        {

        }

        public static HiBlogContext Create()
        {
            return new HiBlogContext();
        }

        

        public System.Data.Entity.DbSet<HiBlog.Models.Post> Posts { get; set; }

        //public System.Data.Entity.DbSet<HiBlog.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<HiBlog.Models.Tag> Tags { get; set; }

   //     object placeHolderVariable;
        public System.Data.Entity.DbSet<HiBlog.Models.TagPost> TagPosts { get; set; }

        public System.Data.Entity.DbSet<HiBlog.Models.Log> Logs { get; set; }
    }
}