﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HiBlog.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "E-Post")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "E-Post on nõutav")]
        [Display(Name = "E-Post")]
        [EmailAddress(ErrorMessage = "Sisesta e-post õigel kujul")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Parool on nõutav")]
        [DataType(DataType.Password)]
        [Display(Name = "Parool")]
        public string Password { get; set; }

        [Display(Name = "Mäleta mind?")]
        public bool RememberMe { get; set; }


    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "E-Post on nõutav")]
        [EmailAddress(ErrorMessage = "Palun sisesta e-post" )]
        [Display(Name = "E-Post")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Parool on nõutav")]
        [StringLength(100, ErrorMessage = " {0} peab olema vähemalt {2} tähemärki pikk, sisaldama ühte sümbolit, suurt tähte ja ühte numbrit.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Parool")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kinnita parool")]
        [Compare("Password", ErrorMessage = "Paroolid ei kattu.")]
        public string ConfirmPassword { get; set; }

        
        public string Name { get; set; }

        [Required(ErrorMessage = "Nime sisestamine on nõutav!")]
        [Display(Name = "Eesnimi")]
        public string FirstName { get; set; }
        [Display(Name = "Perekonna nimi")]
        public string LastName { get; set; }


    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
