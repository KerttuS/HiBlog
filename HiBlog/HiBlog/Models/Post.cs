﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HiBlog.Models
{
    public class Post
    {
        public int Id { get; set; }
        [Display(Name = "Sildid")]
        public ICollection<TagPost> TagPost { get; set; }

        [Required(ErrorMessage = "Pealkiri on nõutav!")]
        [Display(Name = "Pealkiri")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Sisu on nõutav!")]
        [Display(Name = "Sisu")]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Text { get; set; }
        [Display(Name = "Avaldatud")]
        public bool Published { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Postitamise aeg")]
        public DateTime? PostedOn { get; set; }
        public DateTime? Modified { get; set; }
        [Display(Name = "Meeldimiste arv")]
        public int NumberOfLikes { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }

    }
}