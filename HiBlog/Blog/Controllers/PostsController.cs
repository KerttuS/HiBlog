﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HiBlog.Models;
using System.Threading.Tasks;

namespace HiBlog.Controllers
{
    public class PostsController : Controller
    {
        private HiBlogContext db = new HiBlogContext();

        //// GET: Posts
        //public ActionResult Index()
        //{
        //    var posts = db.Posts.OrderByDescending(x => x.PostedOn).Take(5);
        //    return View(posts.ToList());
        //    //return View(db.Posts.ToList());
        //}

        public ActionResult ShowAllPosts()
        {
            return View(db.Posts.ToList());
        }


        // GET: Posts/Details/5
        public ActionResult ReadOn(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // GET: Posts/Create
        public ActionResult Create()
        {
            ViewBag.Tag = new SelectList(db.Tags, "Id", "Name", null);
            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Text,Published,PostedOn/*,Modified*/")] Post post)
        {
            if (ModelState.IsValid)
            {
                //post.Modified = DateTime.Now;
                post.PostedOn = DateTime.Now;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(post);
        }

        // GET: Posts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Text,Published,PostedOn,Modified")] Post post)
        {
            if (ModelState.IsValid)
            {
                post.Modified = DateTime.Now;
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        // GET: Posts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ViewResult Index(string searchString)
        {
            if(searchString == null)
            {
                var posts = db.Posts.OrderByDescending(x => x.PostedOn).Take(5);
                return View(posts.ToList());
            }
            ViewData["CurrentFilter"] = searchString;

            var post = from s in db.Posts
                       select s;

            if (!String.IsNullOrEmpty(searchString))
            {
                post = post.Where(s => s.Text.Contains(searchString)
                                       || s.Title.Contains(searchString));
            }
            return View(post.ToList());
        }
        public ActionResult AddLike (int id)
        {
            var post = db.Posts.Find(id);
            post.NumberOfLikes++;
            db.Entry(post).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        //public ActionResult ShowAllPostsByNumberOfLikes()
        //{
        //    return View(db.Posts.OrderByDescending(x => x.NumberOfLikes).ToList());
        //}

        public ActionResult SortBy(string sortOrder)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Title" : "";
            ViewBag.DateSortParm = sortOrder == "PostedOn" ? "NumberOfLikes" : "PostedOn";
            var posts = from s in db.Posts
                           select s;
            switch (sortOrder)
            {
                case "Title":
                    posts = posts.OrderByDescending(s => s.Title);
                    break;
                case "PostedOn":
                    posts = posts.OrderBy(s => s.PostedOn);
                    break;
                case "NumberOfLikes":
                    posts = posts.OrderByDescending(s => s.NumberOfLikes);
                    break;
                default:
                    posts = posts.OrderBy(s => s.PostedOn);
                    break;
            }
            TempData["SortedPosts"] = posts.ToList();
            return RedirectToAction ("ShowAllPosts");
        }
    }
}
