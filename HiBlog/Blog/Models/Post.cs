﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HiBlog.Models
{
    public class Post
    {
        public int Id { get; set; }
        public ICollection<TagPost> TagPost { get; set; }

        [Required]
        [Display(Name = "Pealkiri")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Sisu")]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Text { get; set; }
        public bool Published { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Postitamise aeg")]
        public DateTime? PostedOn { get; set; }
        public DateTime? Modified { get; set; }
        public int NumberOfLikes { get; set; }
    }
}