﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HiBlog.Models
{
    public class Log
    {
        public int Id { get; set; }
        public string Action { get; set; }
        public DateTime? TimeOfAction { get; set; }

    }
}