﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HiBlog.Models
{ 
    
    public class Tag
    {
        public int Id { get; set; }

        [Display(Name = "Märksõna")]
        public string Name { get; set; }
        //public string Slug { get; set; }

        public ICollection<TagPost> TagPost { get; set; }
    }

    public class TagPost
    {
        [Key, Column(Order = 0)]
        public int PostId { get; set; }
        [Key, Column(Order = 1)]
        public int TagId { get; set; }
        public Post Post { get; set; }
        public Tag Tag { get; set; }
    }
    
}