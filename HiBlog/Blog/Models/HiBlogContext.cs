﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HiBlog.Models
{
    public class HiBlogContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public HiBlogContext() : base("name=HiBlogContext")
        {
        }

        public System.Data.Entity.DbSet<HiBlog.Models.Post> Posts { get; set; }

        public System.Data.Entity.DbSet<HiBlog.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<HiBlog.Models.Tag> Tags { get; set; }

        object placeHolderVariable;

        public System.Data.Entity.DbSet<HiBlog.Models.TagPost> TagPosts { get; set; }

        public System.Data.Entity.DbSet<HiBlog.Models.Tag> Log { get; set; }
    }
}
