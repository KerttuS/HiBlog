﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HiBlog.Models
{
    public class User
    {
        public int Id { get; set; }
        [Range(18, 100, ErrorMessage = "Vanus saab olla vahemikus 18 - 100!")]
        [Required(ErrorMessage = "Nime sisestamine on nõutav!")]
        
        [Display(Name = "Eesnimi")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Nime sisestamine on nõutav!")]
        [Display(Name = "Perekonnanimi")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "E-posti aadressi sisestamine on nõutav!")]
        [Display(Name = "E-posti aadress")]
        [EmailAddress(ErrorMessage = "Sisestatud aarsess ei ole korrektne!")]
        public string EmailAdresse { get; set; }
        [Display(Name = "Kasutajanimi")]
        public string Username { get; set; }
        [StringLength(10, MinimumLength = 5, ErrorMessage = "Parooli pikkus peab olema 5-10 sümbolit")]
        [Required(ErrorMessage = "Salasõna sisestamine on nõutav!")]
        [Display(Name = "Salasõna")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool Administrator { get; set; }
        public bool Blogger { get; set; }
        public bool Reader { get; set; }
        
    }
}