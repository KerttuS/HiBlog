﻿SET IDENTITY_INSERT [dbo].[AuthorsBooks] ON
INSERT INTO [dbo].[AuthorsBooks] ([Id], [AuthorsId], [BooksId]) VALUES (1, 1, 2)
INSERT INTO [dbo].[AuthorsBooks] ([Id], [AuthorsId], [BooksId]) VALUES (2, 2, 2)
INSERT INTO [dbo].[AuthorsBooks] ([Id], [AuthorsId], [BooksId]) VALUES (3, 3, 3)
INSERT INTO [dbo].[AuthorsBooks] ([Id], [AuthorsId], [BooksId]) VALUES (4, 2, 3)
SET IDENTITY_INSERT [dbo].[AuthorsBooks] OFF
